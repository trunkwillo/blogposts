from blockchain_modules.hashfunction import SimpleHashFunction
from blockchain_modules.blockchain import Blockchain

b = Blockchain()
b.addNode("Hello")
b.addNode("World!")
b.prettyPrint()

print(b.validateBlockchain())
