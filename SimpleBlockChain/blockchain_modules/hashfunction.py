class SimpleHashFunction:
    @staticmethod
    def hashValue(s: str, size: int = 4) -> str:
        # Calculate the sum of all the ASCII values
        total = 0
        for c in s:
            total += ord(c)

        # Limit to the expected size
        hash_value = str(total % (10 ** size))

        # Add padding if needed
        for i in range(0, size - len(hash_value)):
            hash_value = "0" + hash_value

        return hash_value