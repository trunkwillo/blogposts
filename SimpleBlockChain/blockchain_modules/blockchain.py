from datetime import datetime
import hashlib

class Block:
    def __init__(self, data, prev_hash):
        self.timestamp = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
        self.data = data
        self.hash = Block.hashValue(self.timestamp, data, prev_hash)
        

    @staticmethod
    def hashValue(timestamp, data, prev_hash):
        if prev_hash:
            return hashlib.sha256((str(timestamp) + str(data) + str(prev_hash)).encode("utf-8")).hexdigest()
        else:
            return hashlib.sha256((str(timestamp) + str(data)).encode("utf-8")).hexdigest()


class Blockchain:
    def __init__(self):
        self.blocks = []
    
    def addNode(self, data) -> None:
        if len(self.blocks) == 0:
            self.blocks.append(Block(data, ""))
        else:
            self.blocks.append(Block(data, self.blocks[-1].hash))

    def getNode(self, index: int) -> Block:
        if index >= len(self.blocks):
            return None
        return self.blocks[index]
    
    def validateBlockchain(self) -> bool:
        if len(self.blocks) == 0: # Empty blockchains are valid
            return True
        
        for i in range(0, len(self.blocks)):
            if i == 0: # First case is a special one
                if Block.hashValue(self.blocks[i].timestamp, self.blocks[i].data, "") != self.blocks[i].hash:
                    return False
            else:
                if Block.hashValue(self.blocks[i].timestamp, self.blocks[i].data, self.blocks[i-1].hash) != self.blocks[i].hash:
                    print(i)
                    return False
        return True


    def prettyPrint(self) -> None:
        for b in self.blocks:
            print("Timestamp: " + str(b.timestamp))
            print("Data: " + str(b.data))
            print("Hash: " + str(b.hash))
            print("--------------\n")